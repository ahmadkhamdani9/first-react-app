import React , {useState} from 'react';
import { StyleSheet , Text , View , Button , TextInput , Modal} from 'react-native';





export default function AddText(props){
    const [textValue , setTextValue] = useState('');

    return (
        <Modal visible={props.visibility} animated={'slide'}>
            <View style={styles.container}>
                <TextInput style={styles.input} placeholder='Input your text' value={textValue} onChangeText={(text) => {setTextValue(text)}} />
                <View style={{flexDirection : 'row' , width : '100%', justifyContent : 'center'}}>
                    <View style={{width : '30%' , marginRight : 20}}>
                        <Button  title="Add" onPress={() => {
                            props.tambahText(textValue);
                            setTextValue('');
                        } } />
                    </View>
                    <View style={{width : '30%'}}>
                        <Button title = "Cancel" color='red' onPress={props.onCancel} />
                    </View>
                </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    container : {
        flex: 1,
        backgroundColor : 'rgb(240,240,240)',
        justifyContent : 'center',
        alignItems : 'center',
        padding : 20
    },
    input : {
        color : 'black',
        width : '90%',
        fontSize : 20,
        padding : 15,
        paddingHorizontal: 15,
        borderBottomColor : 'rgba(0,0,0,0.5)',
        borderBottomWidth: 1,
        marginBottom: 15,
    }
});