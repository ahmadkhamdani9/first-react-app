import React from 'react';
import {StyleSheet ,Text , View , FlatList , TouchableOpacity} from 'react-native';


export default function ListItem(props){
    return (
        <FlatList keyExtractor={(item , index) => item.key} style={{marginTop : 20}} data={props.data} renderItem={(data) => {
            return (
                <TouchableOpacity activeOpacity={0.7} onPress={ () => {
                    props.onDelete(data.item.key);
                } }>
                    <View style={styles.items}>
                        <Text style={styles.text}>{data.item.value}</Text>
                    </View>
                </TouchableOpacity>
            )
        }
        } />
    )
}

const styles = StyleSheet.create({
    items : {
        backgroundColor : 'rgba(230,230,230,1)',
        width : '100%',
        height : 45,
        paddingHorizontal: 10,
        justifyContent: 'center',
        marginBottom: 10,
    },
    text : {
        fontSize : 19
    }
});