import React , {useState} from 'react';
import { StyleSheet, Text, View , Button } from 'react-native';
import AddText from './components/AddText';
import EditText from './components/EditText';
import ListItem from './components/ListItem';
import { NavigationNativeContainer } from '@react-navigation/native';
import { createStackNavigator, StackGestureContext } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { TextInput } from 'react-native-gesture-handler';
// firebase
import * as firebase from 'firebase';
import 'firebase/database';
import firebaseConfig from './components/FirebaseImp';


export default function App() {

  const Stack = createStackNavigator();
  

  // RENDER VIEW
  return (
    <NavigationNativeContainer style={styles.container } >
      <Stack.Navigator initialRouteName="Home" >
        <Stack.Screen name="Home" component={HomeScreen}/>
        <Stack.Screen name="Some" component={SomeScreen}/>
        <Stack.Screen name="Detail" component={DetailScreen}/>
      </Stack.Navigator>
    </NavigationNativeContainer>
  );
  
}

function HomeScreen({navigation}){
  
  if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
  }


  
  var ref = firebase.database().ref('Users');
  
  const getData = function(){
    var bigData = [];

    ref.on('value', (snap) => {
      snap.forEach((childSnap) => {
        var dataNew = childSnap.val();
        bigData.push(dataNew);
      });
    });
    
    return bigData;
  };

  
  // --------------------------------------------------
  const [textData, setTextData] = useState(getData());
  const [modalShow, setModalShow] = useState(false);
  const [editShow, setEditShow] = useState(false);
  

  
  const inputTextData = (text) => {
    var key = null;
    var value = null;
    
    if (text !== '') {
      setTextData((data) => {
        key = Math.random().toString();
        value = text;
        return [...data, {
          key: key,
          value: value
        }];
      })
    }
    
    firebase.database().ref('Users').push({key : key , value : value});
    setModalShow(false);
  }

  const [selectedText , setSelectedText] = useState('');
  const filterItems = (key) => {
    // setTextData(data => {
    //   return data.filter(e => e.key !== key)
    // });
    const dataPass = textData.filter((e) => e.key == key)[0];
    setSelectedText(dataPass);
    setEditShow(true);
  }
  
  const editKalimat = () => {

    setTextData(value => {
      return value.map((e) => {
        return e.key == selectedText.key ? {key : e.key , value : selectedText.value } : e;
      })
    })
    setEditShow(false);
  }

  const deleteKalimat = () => {
    setTextData(data => {
      return data.filter(e => e.key !== selectedText.key)
    });
    setEditShow(false);
  }

  

  return (
      <View style={styles.container} >
        <View style={{width : '100%'}}>
          <Button onPress={() => {setModalShow(true)}} title="Add New Text"/>
        </View>
        <AddText visibility={modalShow} tambahText={inputTextData} onCancel={ () => {setModalShow(false) }}/>
        <EditText nilai={selectedText.value} visibility={editShow} onChangeText={(text) => {setSelectedText({key : selectedText.key , value : text})}}  onDelete={deleteKalimat} onEdit={editKalimat}/>
        <View style={{marginTop : 10}}>
          <Button title="Change Screen" onPress={() => {navigation.navigate('Some' , {name : 'Rizal'})}} color='rgb(120,120,150)' />
        </View>
        <ListItem data={textData} onDelete={filterItems} />
      </View>
  );
}

function SomeScreen({route , navigation}){
  const {name} = route.params;
  return (
      <View style={styles.container}>
        <Text>{name}</Text>
      </View>
  );
}


function DetailScreen({route , navigation}){
  let {text} = route.params;
  return (
      <View style={styles.container2}>
        <View style={styles.editWrap}>
          <TextInput style={styles.editText} value={text.value}/>
        </View>
      </View>
  );
}

// CSS
const styles = StyleSheet.create({
  container: {
    paddingTop: 10,
    height : '100%',
    backgroundColor: 'white',
    paddingHorizontal:  20,
  },
  container2 : {
    paddingTop: 10,
    height: '100%',
    backgroundColor: 'white',
    paddingHorizontal: 20,
    justifyContent : 'center',
    alignItems : 'center'
  },
  editWrap : {
    padding : 10,
    borderBottomWidth: 1,
    borderBottomColor : 'black'
  },
  editText : {
    fontSize : 20,
  }
});
